import React, { useEffect } from 'react';
import './App.css';

export default function App() {
  useEffect(() => {shuffle()})

  function shuffle() {
    swapTiles('cell11', 'cell21');
    swapTiles('cell11', 'cell22');
    swapTiles('cell12', 'cell23');
    swapTiles('cell13', 'cell31');
    swapTiles('cell23', 'cell32');
    swapTiles('cell31', 'cell32');
  }
  
function swapTiles(cell1,cell2) {
  var temp = document.getElementById(cell1).className;
  // find element with id
  document.getElementById(cell1).className = document.getElementById(cell2).className;
  document.getElementById(cell2).className = temp;
}

function clickTile(row,column) {
  var element = document.getElementById("cell"+row+column);
    var tile = element.className;
  if (tile!=="tile9") { 
    //Checking if white tile on the right
    if (column<3) {
      if ( document.getElementById("cell"+row+(column+1)).className==="tile9") {
        swapTiles("cell"+row+column,"cell"+row+(column+1));
        checkForWin();
        return;
      }
    }
    //Checking if white tile on the left
    if (column>1) {
      if ( document.getElementById("cell"+row+(column-1)).className==="tile9") {
        swapTiles("cell"+row+column,"cell"+row+(column-1));
        checkForWin();
        return;
      }
    }
      //Checking if white tile is above
    if (row>1) {
      if ( document.getElementById("cell"+(row-1)+column).className==="tile9") {
        swapTiles("cell"+row+column,"cell"+(row-1)+column);
        checkForWin();
        return;
      }
    }
    //Checking if white tile is below
    if (row<3) {
      if ( document.getElementById("cell"+(row+1)+column).className==="tile9") {
        swapTiles("cell"+row+column,"cell"+(row+1)+column);
        checkForWin();
        return;
      }
    } 
  }
}

function checkForWin() {
  console.log(document.getElementById("cell11").className, document.getElementById("cell12").className, document.getElementById("cell13").className, document.getElementById("cell21").className, document.getElementById("cell22").className, document.getElementById("cell23").className, document.getElementById("cell31").className, document.getElementById("cell32").className, document.getElementById("cell33").className)
  if (document.getElementById("cell11").className !== 'tile1') return;
  if (document.getElementById("cell12").className !== 'tile2') return;
  if (document.getElementById("cell13").className !== 'tile3') return;
  if (document.getElementById("cell21").className !== 'tile4') return;
  if (document.getElementById("cell22").className !== 'tile5') return;
  if (document.getElementById("cell23").className !== 'tile6') return;
  if (document.getElementById("cell31").className !== 'tile7') return;
  if (document.getElementById("cell32").className !== 'tile8') return;
  document.getElementById('table').className = 'tablewin';
  document.getElementById('stop_inspecting_you_nosy_dev').className = 'tilewin';
}

  return (
<div>
<div id="table">
   <div id="row1">
      <div id="cell11" className='tile1' onClick={() => clickTile(1,1)}></div>
      <div id="cell12" className='tile2' onClick={() => clickTile(1,2)}></div>
      <div id="cell13" className='tile3' onClick={() => clickTile(1,3)}></div>
   </div>
   <div id="row2">
      <div id="cell21" className='tile4' onClick={() => clickTile(2,1)}></div>
      <div id="cell22" className='tile5' onClick={() => clickTile(2,2)}></div>
      <div id="cell23" className='tile6' onClick={() => clickTile(2,3)}></div>
   </div>
   <div id="row3">
      <div id="cell31" className='tile7' onClick={() => clickTile(3,1)}></div>
      <div id="cell32" className='tile8' onClick={() => clickTile(3,2)}></div>
      <div id="cell33" className='tile9' onClick={() => clickTile(3,3)}></div>
   </div>
</div>
<div id="stop_inspecting_you_nosy_dev"></div>
</div>
  );
}
